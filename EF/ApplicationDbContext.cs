﻿using DoVT2Mail_Data.Domains.EF.Configurations;
using DoVT2Mail_Data.Domains.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace DoVT2Mail_Data.Domains.EF;

public class ApplicationDbContext : DbContext
{
#pragma warning disable
    public ApplicationDbContext(DbContextOptions options) : base(options) { }
#pragma warning restore

    ///<summary> to config instead of program.cs
    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        var configuration = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json")
            .Build();
        var connectionString = configuration.GetConnectionString("DokuroSQLConnectionString");
        Console.WriteLine(connectionString);

        //* comment this to run migrations
        // optionsBuilder.UseSqlServer(connectionString);
        // optionsBuilder.UseInMemoryDatabase("DokuroInMemoryDB");

        // optionsBuilder.UseLazyLoadingProxies();
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        base.OnModelCreating(modelBuilder);
        // fluentAPI
        modelBuilder.ApplyConfiguration(new DateTimeTimeZoneConfiguration());
        modelBuilder.ApplyConfiguration(new EmailAddressConfiguration());
        modelBuilder.ApplyConfiguration(new EventConfiguration());
        modelBuilder.ApplyConfiguration(new GroupConfiguration());
        modelBuilder.ApplyConfiguration(new GroupMemberConfiguration());
        modelBuilder.ApplyConfiguration(new ItemBodyConfiguration());
        modelBuilder.ApplyConfiguration(new MessageConfiguration());
        modelBuilder.ApplyConfiguration(new RecipientConfiguration());

        // seed
        modelBuilder.Seed();
    }

    public DbSet<DateTimeTimeZone> DateTimeTimeZones { get; set; }
    public DbSet<EmailAddress> EmailAddresses { get; set; }
    public DbSet<Event> Events { get; set; }
    public DbSet<Group> Groups { get; set; }
    public DbSet<GroupMember> GroupMembers { get; set; }
    public DbSet<ItemBody> ItemBodies { get; set; }
    public DbSet<Message> Messages { get; set; }
    public DbSet<Recipient> Recipients { get; set; }
}

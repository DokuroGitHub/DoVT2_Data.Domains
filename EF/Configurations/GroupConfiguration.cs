﻿using DoVT2Mail_Data.Domains.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DoVT2Mail_Data.Domains.EF.Configurations
{
    public class GroupConfiguration : IEntityTypeConfiguration<Group>
    {
        public void Configure(EntityTypeBuilder<Group> builder)
        {
            builder.ToTable("Group");
            builder.HasKey(x => x.Id);
            builder
                .Property(x => x.Id)
                .ValueGeneratedOnAdd();
            builder
                .HasIndex(x => x.Mail)
                .IsUnique();
            // ref
            builder
                .HasOne(x => x.MemberOfGroup)
                .WithOne()
                .HasForeignKey<Group>(x => x.MemberOfGroupId);
            builder
                .HasMany(x => x.GroupMembers)
                .WithOne(x => x.Group)
                .HasForeignKey(x => x.GroupId);

        }
    }
}

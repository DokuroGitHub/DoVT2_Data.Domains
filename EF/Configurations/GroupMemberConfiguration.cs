﻿using DoVT2Mail_Data.Domains.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DoVT2Mail_Data.Domains.EF.Configurations
{
    public class GroupMemberConfiguration : IEntityTypeConfiguration<GroupMember>
    {
        public void Configure(EntityTypeBuilder<GroupMember> builder)
        {
            builder.ToTable("GroupMember");
            builder.HasKey(x => new { x.GroupId, x.EmailAddressId });
            builder
                .Property(x => x.GroupId)
                .ValueGeneratedOnAdd();
            builder
                .Property(x => x.EmailAddressId)
                .ValueGeneratedOnAdd();
            // ref
            builder
                .HasOne(x => x.Group)
                .WithMany(x => x.GroupMembers)
                .HasForeignKey(x => x.GroupId);
            builder
                .HasOne(x => x.EmailAddress)
                .WithOne()
                .HasForeignKey<GroupMember>(x => x.EmailAddressId);
        }
    }
}

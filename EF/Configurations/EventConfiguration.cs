﻿using DoVT2Mail_Data.Domains.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DoVT2Mail_Data.Domains.EF.Configurations
{
    public class EventConfiguration : IEntityTypeConfiguration<Event>
    {
        public void Configure(EntityTypeBuilder<Event> builder)
        {
            builder.ToTable("Event");
            builder.HasKey(x => x.Id);
            builder
                .Property(x => x.Id)
                .ValueGeneratedOnAdd();
            // ref
            builder
                .HasOne(x => x.Start)
                .WithOne()
                .HasForeignKey<Event>(x => x.StartId);
            builder
                .HasOne(x => x.End)
                .WithOne()
                .HasForeignKey<Event>(x => x.EndId);
        }
    }
}

﻿using DoVT2Mail_Data.Domains.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DoVT2Mail_Data.Domains.EF.Configurations
{
    public class RecipientConfiguration : IEntityTypeConfiguration<Recipient>
    {
        public void Configure(EntityTypeBuilder<Recipient> builder)
        {
            builder.ToTable("Recipient");
            builder.HasKey(x => new { x.MessageId, x.EmailAddressId });
            builder
                .Property(x => x.MessageId)
                .ValueGeneratedOnAdd();
            builder
                .Property(x => x.EmailAddressId)
                .ValueGeneratedOnAdd();
            // ref
            builder
                .HasOne(x => x.Message)
                .WithMany(x => x.Recipients)
                .HasForeignKey(x => x.MessageId);
            // builder.Ignore(x => x.Message);
            builder
                .HasOne(x => x.EmailAddress)
                .WithOne()
                .HasForeignKey<Recipient>(x => x.EmailAddressId);
        }
    }
}

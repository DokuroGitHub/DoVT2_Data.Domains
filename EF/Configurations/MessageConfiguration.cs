﻿using DoVT2Mail_Data.Domains.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DoVT2Mail_Data.Domains.EF.Configurations
{
    public class MessageConfiguration : IEntityTypeConfiguration<Message>
    {
        public void Configure(EntityTypeBuilder<Message> builder)
        {
            builder.ToTable("Message");
            builder.HasKey(x => x.Id);
            builder
                .Property(x => x.Id)
                .ValueGeneratedOnAdd();
            // ref
            builder
                .HasOne(x => x.Body)
                .WithOne()
                .HasForeignKey<Message>(x => x.BodyId);
            builder.Ignore(x => x.From);
            builder
                .HasMany(x => x.Recipients)
                .WithOne(x => x.Message)
                .HasForeignKey(x => x.MessageId);
        }
    }
}

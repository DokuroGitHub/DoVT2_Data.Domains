﻿using DoVT2Mail_Data.Domains.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DoVT2Mail_Data.Domains.EF.Configurations
{
    public class ItemBodyConfiguration : IEntityTypeConfiguration<ItemBody>
    {
        public void Configure(EntityTypeBuilder<ItemBody> builder)
        {
            builder.ToTable("ItemBody");
            builder.HasKey(x => x.Id);
            builder
                .Property(x => x.Id)
                .ValueGeneratedOnAdd();
            // ref
        }
    }
}

﻿using DoVT2Mail_Data.Domains.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DoVT2Mail_Data.Domains.EF.Configurations;

public class DateTimeTimeZoneConfiguration : IEntityTypeConfiguration<DateTimeTimeZone>
{
    public void Configure(EntityTypeBuilder<DateTimeTimeZone> builder)
    {
        builder.ToTable("DateTimeTimeZone");
        builder.HasKey(x => x.Id);
        builder
            .Property(x => x.Id)
            .ValueGeneratedOnAdd();
        // ref
    }
}

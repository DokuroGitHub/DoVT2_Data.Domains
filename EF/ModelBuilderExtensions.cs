using DoVT2Mail_Data.Domains.Models;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace DoVT2Mail_Data.Domains.EF;

public static class ModelBuilderExtensions
{
    public static void Seed(this ModelBuilder modelBuilder)
    {
        modelBuilder.GenericSeed<EmailAddress>("Seeds/EmailAddress.json");

        // modelBuilder.Entity<EmailAddress>().HasData(
        //     new EmailAddress
        //     {
        //         Id = "EmailAddress_1",
        //         Name = "EmailAddress 1",
        //         Name = "EmailAddress_1@gmail.com"
        //     },
        // );
    }

    public static void GenericSeed<T>(this ModelBuilder modelBuilder, string path) where T : class
    {
        using (var r = new StreamReader(path))
        {
            string json = r.ReadToEnd();
            var items = JsonConvert.DeserializeObject<List<T>>(json);
            if (items != null)
            {
                modelBuilder.Entity<T>().HasData(items);
            }
        }
    }
}

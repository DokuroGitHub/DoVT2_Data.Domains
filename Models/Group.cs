﻿namespace DoVT2Mail_Data.Domains.Models;

public class Group
{
    public string Id { get; set; }
    public string? DisplayName { get; set; }
    public string? Description { get; set; }
    public DateTimeOffset? CreatedDateTime { get; set; }
    public DateTimeOffset? DeletedDateTime { get; set; }
    public string Mail { get; set; }
    public string? MailNickname { get; set; }
    public string? MemberOfGroupId { get; set; }
    // ref
    public ICollection<GroupMember> GroupMembers { get; set; } // 1:8
    public Group? MemberOfGroup { get; set; } // 8:1
}

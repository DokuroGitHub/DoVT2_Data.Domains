﻿namespace DoVT2Mail_Data.Domains.Models;

public class Recipient
{
    public string MessageId { get; set; }
    public string EmailAddressId { get; set; }
    // From/To/CC/BCC/ReplyTo
    public string Type { get; set; }
    // ref
    public virtual Message Message { get; set; }
    public virtual EmailAddress EmailAddress { get; set; }
    // public virtual ICollection<Message> Messages { get; set; }
}

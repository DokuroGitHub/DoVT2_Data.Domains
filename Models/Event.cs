namespace DoVT2Mail_Data.Domains.Models;

public class Event
{
    public string Id { get; set; }
    public string? CreatedDateTime { get; set; }
    public string? LastModifiedDateTime { get; set; }
    public string? Subject { get; set; }
    public string? StartId { get; set; }
    public string? EndId { get; set; }
    // ref
    public DateTimeTimeZone? Start { get; set; }
    public DateTimeTimeZone? End { get; set; }
}
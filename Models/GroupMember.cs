﻿namespace DoVT2Mail_Data.Domains.Models;

public class GroupMember
{
    public string GroupId { get; set; }
    public string EmailAddressId { get; set; }
    // ref
    public virtual Group Group { get; set; }
    public virtual EmailAddress EmailAddress { get; set; }
}

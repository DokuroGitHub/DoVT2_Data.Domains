﻿namespace DoVT2Mail_Data.Domains.Models;

public class Message
{
    public string Id { get; set; }
    public string FromId { get; set; }
    public string BodyId { get; set; }
    public DateTime CreatedDateTime { get; set; }
    public DateTime? SentDateTime { get; set; }
    public string Subject { get; set; }
    // ref
    public virtual ItemBody Body { get; set; } // 1:1
    public virtual Recipient From { get; set; } // 8:1
    public virtual ICollection<Recipient> Recipients { get; set; } // 8:8
}

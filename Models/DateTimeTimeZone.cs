namespace DoVT2Mail_Data.Domains.Models;

public class DateTimeTimeZone
{
    public string Id { get; set; }
    public string DateTime { get; set; }
    public string TimeZone { get; set; }
}
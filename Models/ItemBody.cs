﻿namespace DoVT2Mail_Data.Domains.Models;

public class ItemBody
{
    public string Id { get; set; }
    public string ContentType { get; set; }
    public string Content { get; set; }
}

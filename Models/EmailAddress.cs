﻿namespace DoVT2Mail_Data.Domains.Models;

public class EmailAddress
{
    public string Id { get; set; }
    public string? Name { get; set; }
    public string Address { get; set; }
}
